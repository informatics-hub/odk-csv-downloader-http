/**
 * Automatically generated file. DO NOT MODIFY
 */
package net.globalhealthapp.odkcsvdownloadhttp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "net.globalhealthapp.odkcsvdownloadhttp";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.1";
}
